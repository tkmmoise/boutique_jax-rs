/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class Personne  {
    
    protected Long id;
    protected String nom;
    protected String prenom;
    protected LocalDate dateNaissance;
    
    
    public Personne() {
    }
    
    //getters et setters
    public Long getId(){
        return this.id;
    }
    public String getNom(){
        return this.nom;
    }
    public String getPrenom(){
        return this.prenom;
    }
    public LocalDate getDateNaissance(){
        return this.dateNaissance;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    // methodes
    public int getAge() {
        return LocalDate.now().getYear() - this.dateNaissance.getYear();
    }
    public int getAge(LocalDate dateReference){
        if(dateReference.isBefore(this.dateNaissance)){
            return this.dateNaissance.getYear();
        }
        return dateReference.getYear() - this.dateNaissance.getYear();
    }
    
    //methodes redefinies
    public boolean equals(Personne p) {
        return Objects.equals(this.id, p.getId());
    }

    public int hashCode() {
        return Objects.hash(id, nom, prenom, dateNaissance);
    }

    public String toString(){
        return "id = "+this.id + " Nom = "+this.nom+" Prenom = "+this.prenom+ " Date de Naissance = "+ this.dateNaissance;
    }
    
}
