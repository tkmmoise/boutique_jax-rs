/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.util.Objects;

/**
 *
 * @author TKM
 */
public class Categorie {
    
    private Integer id;
    private String libelle;
    private String description;
  
    public Categorie() {
    }

    //getters et setters
    public Integer getId() {
        return id;
    }
    public String getLibelle() {
        return libelle;
    }
    public String getDescription() {
        return description;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    //methodes redefinies
    public boolean equals(Categorie c) {
        return Objects.equals(this.id, c.getId());
    }

    public int hashCode() {
        return Objects.hash(id, libelle, description);
    }

    public String toString() {
        return "Categorie{" + "id=" + id + ", libelle=" + libelle + ", description=" + description + '}';
    }
}
