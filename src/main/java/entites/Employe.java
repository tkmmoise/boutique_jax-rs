/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.beans.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class Employe extends Personne{
    
    private String cnss;
   
    public Employe() {
        
    }
    
    //getters et setters
    public String getCnss() {
        return cnss;
    }
   
    public void setCnss(String cnss) {
        this.cnss = cnss;
    }
    
    //methodes redefinies
    public boolean equals( Employe e) {
        return Objects.equals(this.id, e.getId());
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), cnss);
    }

    public String toString() {
        return "id = "+this.id + " Nom = "+this.nom+" Prenom = "+this.prenom+ " Date de Naissance = "+ this.dateNaissance + " Employe " + "cnss=" + this.cnss;

    }
    

    
}
