/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.beans.*;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class ProduitAchete implements Serializable {
    
    private int quantite;
    private double remise;
    private Produit produit;
    private Achat achat;
    
    public ProduitAchete() {
        this.quantite = 1;
        this.remise = 0;
    }

    //getters et setters
    public int getQuantite() { return quantite; }
    public double getRemise() { return remise; }
    public Produit getProduit() { return produit; }
    public Achat getAchat() { return achat; }

    public void setQuantite(int quantite) { this.quantite = quantite; }
    public void setRemise(double remise) { this.remise = remise; }
    public void setProduit(Produit produit) { this.produit = produit; }
    public void setAchat(Achat achat) { this.achat = achat; }

    //methodes redefinies
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProduitAchete that = (ProduitAchete) o;
        return quantite == that.quantite && Double.compare(that.remise, remise) == 0 && produit.equals(that.produit) && achat.equals(that.achat);
    }

    public int hashCode() {
        return Objects.hash(quantite, remise, produit, achat);
    }

    public String toString() {
        return "ProduitAchete{" + " quantite= " + quantite +", remise= " + remise +", produit= " + produit +", achat= " + achat +'}';
    }

    public double getPrixTotal(){
        return (this.produit.getPrixUnitaire()*this.quantite) - this.remise;
    }
    
}
