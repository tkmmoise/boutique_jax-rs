/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class Achat{
    
    private Long id;
    private double remise;
    private LocalDateTime dateAchat;
    private List<ProduitAchete> listProduits = new LinkedList<>();
    
    public Achat() {
        this.remise =0;
    }

    //getters et setters
    public Long getId() {
        return id;
    }
    public double getRemise() {
        return remise;
    }
    public LocalDateTime getDateAchat() {
        return dateAchat;
    }
    
    public List<ProduitAchete> getListProduits() {
        return listProduits;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public void setRemise(double remise) {
        this.remise = remise;
    }
    public void setDateAchat(LocalDateTime dateAchat) {
        this.dateAchat = dateAchat;
    }
    
    public void setListProduits(ArrayList<ProduitAchete> listProduits) {
        this.listProduits = listProduits;
    }

    //autres methodes
    public boolean equals(Achat a) {
        return Objects.equals(this.id, a.getId());
    }

    public int hashCode() {
        return Objects.hash(id, remise, dateAchat ,listProduits);
    }

    public String toString() {
        return "Achat{" +
                "id=" + id +
                ", remise=" + remise +
                ", dateAchat=" + dateAchat +
                '}';
    }

    public double getRemiseTotal() {
        double remiseTotale=0;
        for(ProduitAchete produitachete : listProduits){
            remiseTotale += produitachete.getRemise();
        }
        return remiseTotale;
    }

    public double getPrixTotal() {
        double prixTotale=0;
        for(ProduitAchete produitachete : listProduits){
            prixTotale += produitachete.getPrixTotal();
        }
        return (prixTotale-this.remise)<0 ? 0 : prixTotale ;
    }
    
    
}
