/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.util.Objects;

/**
 *
 * @author TKM
 */
public class Client extends Personne {
    
    private String cin;
    private String carteVisa;
    
    
    public Client() {
        
    }
    
    //getters et setters
    public String getCarteVisa() {
        return carteVisa;
    }
    public String getCin() {
        return cin;
    }
    public void setCin(String cin) {
        this.cin = cin;
    }
    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }


    public boolean equals( Client c) {
       return this.id == c.getId();
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), carteVisa);
    }

    public String toString() {
        return "id = "+this.id + " Nom = "+this.nom+" Prenom = "+this.prenom+ " Date de Naissance = "+ this.dateNaissance + " cin= "+this.cin+" carteVisa=" + carteVisa;
    }
    
       
}
