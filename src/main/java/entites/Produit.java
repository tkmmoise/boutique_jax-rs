/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class Produit {
    
    private Long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie;
    
    public Produit() {
        
    }

    //getters et setters
    public Long getId() { return id; }
    public String getLibelle() {
        return libelle;
    }
    public double getPrixUnitaire() {
        return prixUnitaire;
    }
    public LocalDate getDatePeremption() {
        return datePeremption;
    }
    public Categorie getCategorie() {
        return categorie;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }
    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
    public void setId(Long id) { this.id = id; }


    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.libelle);
        hash = 73 * hash + (int) (Double.doubleToLongBits(this.prixUnitaire) ^ (Double.doubleToLongBits(this.prixUnitaire) >>> 32));
        hash = 73 * hash + Objects.hashCode(this.datePeremption);
        return hash;
    }

    public boolean equals(Produit p) {
        return Objects.equals(this.id, p.getId());
    }

    public String toString() {
        return "Produit{" + "id=" + id + ", libelle=" + libelle + ", prixUnitaire=" + prixUnitaire + ", datePeremption=" + datePeremption + ", categorie=" + categorie + '}';
    }


    public boolean estPerime(){
        return this.datePeremption.isBefore(LocalDate.now());
    }
    public boolean  estPerime(LocalDate dateReference){
        return this.datePeremption.isBefore(dateReference);
    }
}
