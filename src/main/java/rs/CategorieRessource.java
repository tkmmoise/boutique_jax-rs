/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Categorie;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.CategorieService;

/**
 *
 * @author TKM
 */
@Path("/categorie")
public class CategorieRessource {
    

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Categorie c) {
        // ajoute l'objet e dans la collection liste
        if(c.getId() == null || c.getLibelle() ==null || c.getDescription() == null){
            throw new IllegalArgumentException("Un des champs id,libelle,description est vide");
        }
        
        CategorieService.ajouter(c);
        System.out.println("Add successfull");
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Categorie c) {
        // remplace par e, l'objet Categorie de la liste qui a même id que e
        if(c.getId() == null || c.getLibelle() ==null || c.getDescription() == null){
            throw new IllegalArgumentException("Un des champs id,libelle,description est vide");
        }
        
        CategorieService.modifier(c);
    }

    @GET
    @Path("/{id}")
    public Categorie trouver(@PathParam("id") Integer id) {
        // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        return CategorieService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Integer id) {
        // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        CategorieService.supprimer(id);
    }

    @DELETE
    @Path("/delete")
    public void supprimer(Categorie c) {
        // retirer de la liste, l'objet Categorie passé en paramètre
        if(c.getId() == null || c.getLibelle() ==null || c.getDescription() == null){
            throw new IllegalArgumentException("Un des champs id,libelle,description est vide");
        }
        
        CategorieService.supprimer(c);
    }

    @GET
    public List<Categorie> lister() {
        return CategorieService.lister();
    }

    @GET
    @Path("/getbyparams")
    public List<Categorie> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return CategorieService.lister(debut, nombre);
    }
}
