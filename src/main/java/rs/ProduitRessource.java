/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Produit;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ProduitService;

/**
 *
 * @author TKM
 */
@Path("/produit")
public class ProduitRessource {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void ajouter(Produit p) {
        // ajoute l'objet p dans la collection liste
        if(p.getId()==null||p.getLibelle()==null||p.getPrixUnitaire()==0||p.getDatePeremption()==null||p.getCategorie()==null){
            throw new IllegalArgumentException("Un des  champs id,libelle,description,prixUnitaire,datePeremption,Categorie est vide");
        }
        
        ProduitService.ajouter(p);
        System.out.println("Add successfull");
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Produit p) {
        // remplace par e, l'objet Produit de la liste qui a même id que e
        if(p.getId()==null||p.getLibelle()==null||p.getPrixUnitaire()==0||p.getDatePeremption()==null||p.getCategorie()==null){
            throw new IllegalArgumentException("Un des  champs id,libelle,description,prixUnitaire,datePeremption,Categorie est vide");
        }
        
        ProduitService.modifier(p);
        System.out.println("Modify successfull");
    }

    @GET
    @Path("/{id}")
    public Produit trouver(@PathParam("id") Long id) {
        // renvoie l'objet Produit de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        return ProduitService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id) {
        // retirer de la liste, l'objet Produit qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        ProduitService.supprimer(id);
        System.out.println("Delete successfull");
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/delete")
    public void supprimer(Produit p) {
        // retirer de la liste, l'objet Categorie passé en paramètre
        if(p.getId()==null||p.getLibelle()==null||p.getPrixUnitaire()==0||p.getDatePeremption()==null||p.getCategorie()==null){
            throw new IllegalArgumentException("Un des  champs id,libelle,description,prixUnitaire,datePeremption,Categorie est vide");
        }
        
        ProduitService.supprimer(p);
    }

    @GET
    public List<Produit> lister() {
        return ProduitService.lister();
    }

    @GET
    @Path("/getbyparams")
    public List<Produit> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return ProduitService.lister(debut, nombre);
    }
}
