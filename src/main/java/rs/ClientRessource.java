/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;


import entites.Client;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ClientService;

/**
 *
 * @author TKM
 */
@Path("/client")
public class ClientRessource {
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Client c) {
        // ajoute l'objet e dans la collection liste
        if(c.getId() == null || c.getNom() ==null || c.getPrenom() == null || c.getDateNaissance() == null || c.getCin() == null ||  c.getCarteVisa()==null){
            throw new IllegalArgumentException("Un des champs id,nom,prenom,dateNaissance,cin, est vide");
        }
        
        ClientService.ajouter(c);
        System.out.println("Add successfull");
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Client c) {
        // remplace par e, l'objet Client de la liste qui a même id que e
        if(c.getId() == null || c.getNom() ==null || c.getPrenom() == null || c.getDateNaissance() == null || c.getCin() == null ||  c.getCarteVisa()==null){
            throw new IllegalArgumentException("Un des champs id,nom,prenom,dateNaissance,cin, est vide");
        }
        
        ClientService.modifier(c);
    }

    @GET
    @Path("/{id}")
    public Client trouver(@PathParam("id") Long id) {
        // renvoie l'objet Client de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        return ClientService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id) {
        // retirer de la liste, l'objet Client qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        ClientService.supprimer(id);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/delete")
    public void supprimer(Client c) {
        // retirer de la liste, l'objet Client passé en paramètre
        if(c.getId() == null || c.getNom() ==null || c.getPrenom() == null || c.getDateNaissance() == null || c.getCin() == null ||  c.getCarteVisa()==null){
            throw new IllegalArgumentException("Un des champs id,nom,prenom,dateNaissance,cin, est vide");
        }
        ClientService.supprimer(c);
    }

    @GET
    public List<Client> lister() {
        return ClientService.lister();
    }

    @GET
    @Path("/getbyparams")
    public List<Client> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return ClientService.lister(debut, nombre);
    }
    
}
