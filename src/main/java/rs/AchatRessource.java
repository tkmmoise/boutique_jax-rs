/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Achat;
import entites.Categorie;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.AchatService;
import service.CategorieService;

/**
 *
 * @author TKM
 */
@Path("/achat")
public class AchatRessource {
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouter(Achat a) {
        // ajoute l'objet e dans la collection liste
        if(a.getId() == null  || a.getDateAchat() == null || a.getListProduits()==null ){
            throw new IllegalArgumentException("Un des champs id,dateAchat est vide");
        }
        
        AchatService.ajouter(a);
        System.out.println("Add successfull");
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Achat a) {
        // remplace par e, l'objet Achat de la liste qui a même id que e
        if(a.getId() == null  || a.getDateAchat() == null || a.getListProduits()==null ){
            throw new IllegalArgumentException("Un des champs id,dateAchat est vide");
        }
        
        AchatService.modifier(a);
        System.out.println("Modify successfull");
    }

    @GET
    @Path("/{id}")
    public Achat trouver(@PathParam("id") Long id) {
        // renvoie l'objet Achat de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        return AchatService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id) {
        // retirer de la liste, l'objet Achat qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        AchatService.supprimer(id);
    }

    @DELETE
    @Path("/delete")
    public void supprimer(Achat a) {
        // retirer de la liste, l'objet Achat passé en paramètre
        if(a.getId() == null  || a.getDateAchat() == null || a.getListProduits()==null ){
            throw new IllegalArgumentException("Un des champs id,dateAchat est vide");
        }
        
        AchatService.supprimer(a);
    }

    @GET
    public List<Achat> lister() {
        return AchatService.lister();
    }

    @GET
    @Path("/getbyparams")
    public List<Achat> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return AchatService.lister(debut, nombre);
    }
    
}
