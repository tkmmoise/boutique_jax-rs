/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Produit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 *
 * @author TKM
 */
public class ProduitService {

    static List<Produit> liste = new ArrayList<>();


    public static void ajouter(Produit p) {
        // ajoute l'objet e dans la collection liste
        if(p == null){
            throw new IllegalArgumentException("L'ojbet Produit est null ");
        }
        liste.add(p);
    }

    public static void modifier(Produit p) {
        // remplace par e, l'objet Produit de la liste qui a même id que e
        if(p == null){
            throw new IllegalArgumentException("L'ojbet Produit est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), p.getId())){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet categorie avec l'id " +p.getId()+ " n'est pas trouvé");
        }
        liste.set(pos, p);
    }

    public static Produit trouver(Long id) {
        // renvoie l'objet Produit de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id Produit est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Produit avec l'id " +id+ " n'est pas trouvé");
        }
        
        return liste.get(pos);
    }

    public static void supprimer(Long id) {
        // retirer de la liste, l'objet Produit qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id Produit est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Produit avec l'id " +id+ " n'est pas trouvé");
        }
        
        liste.remove(pos);
    }

    public static void supprimer(Produit p) {
        // retirer de la liste, l'objet Produit passé en paramètre
        supprimer(p.getId());
    }

    public static List<Produit> lister() {
        // renvoyer tous les éléments de la liste
        if(liste == null){
            throw new IllegalArgumentException("La liste Produit est vide");
        }
        return liste;
    }

    public static List<Produit> lister(int debut, int nombre) {
        // renvoyer nombre éléments de la liste, commençant à la position debut
        if(liste == null){
            throw new IllegalArgumentException("La liste Produit est vide");
        }
        if(debut>liste.size() || nombre < 0){
            throw new IllegalArgumentException("Parametes inavalides");
        }
        List<Produit> newliste = new ArrayList();
        int taille = (debut+nombre)>liste.size()?liste.size():(debut+nombre);
        for (int i = debut; i < taille ; i++) {
            newliste.add(liste.get(i));
        }
        return newliste;
    }

}
