/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Client;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class ClientService {
     static List<Client> liste = new ArrayList<>();

    

    public static void ajouter(Client e) {
        // ajoute l'objet e dans la collection liste
        if(e == null){
            throw new IllegalArgumentException("L'ojbet Client est null ");
        }
        liste.add(e);
    }

    public static void modifier(Client e) {
        // remplace par e, l'objet Client de la liste qui a même id que e
        if(e == null){
            throw new IllegalArgumentException("L'ojbet Client est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), e.getId())){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Client avec l'id " +e.getId()+ " n'est pas trouvé");
        }
        liste.set(pos, e);
    }

    public static Client trouver(Long id) {
        // renvoie l'objet Client de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id Client est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Client avec l'id " +id+ " n'est pas trouvé");
        }
        
        return liste.get(pos);
    }

    public static void supprimer(Long id) {
        // retirer de la liste, l'objet Client qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id Client est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Client avec l'id " +id+ " n'est pas trouvé");
        }
        
        liste.remove(pos);
    }

    public static void supprimer(Client e) {
        // retirer de la liste, l'objet Client passé en paramètre
        supprimer(e.getId());
    }

    public static List<Client> lister() {
        // renvoyer tous les éléments de la liste
        if(liste == null){
            throw new IllegalArgumentException("La liste Client est vide");
        }
        return liste;
    }

    public static List<Client> lister(int debut, int nombre) {
        // renvoyer nombre éléments de la liste, commençant à la position debut
        if(liste == null){
            throw new IllegalArgumentException("La liste Client est vide");
        }
        if(debut>liste.size() || nombre < 0){
            throw new IllegalArgumentException("Parametes inavalides");
        }
        List<Client> newliste = new ArrayList();
        int taille = (debut+nombre)>liste.size()?liste.size():(debut+nombre);
        for (int i = debut; i < taille ; i++) {
            newliste.add(liste.get(i));
        }
        return newliste;
    }
}
