/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Categorie;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 *
 * @author TKM
 */
public class CategorieService {

    static List<Categorie> liste = new ArrayList<>();

    public CategorieService() {
    }
    

    public static void ajouter(Categorie e) {
        // ajoute l'objet e dans la collection liste
        if(e == null){
            throw new IllegalArgumentException("L'ojbet categorie est null ");
        }
        liste.add(e);
    }

    public static void modifier(Categorie e) {
        // remplace par e, l'objet Categorie de la liste qui a même id que e
        if(e == null){
            throw new IllegalArgumentException("L'ojbet categorie est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), e.getId())){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet categorie avec l'id " +e.getId()+ " n'est pas trouvé");
        }
        liste.set(pos, e);
    }

    public static Categorie trouver(Integer id) {
        // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id categorie est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet categorie avec l'id " +id+ " n'est pas trouvé");
        }
        
        return liste.get(pos);
    }

    public static void supprimer(Integer id) {
        // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id categorie est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet categorie avec l'id " +id+ " n'est pas trouvé");
        }
        
        liste.remove(pos);
    }

    public static void supprimer(Categorie e) {
        // retirer de la liste, l'objet Categorie passé en paramètre
        supprimer(e.getId());
    }

    public static List<Categorie> lister() {
        // renvoyer tous les éléments de la liste
        if(liste == null){
            throw new IllegalArgumentException("La liste categorie est vide");
        }
        return liste;
    }

    public static List<Categorie> lister(int debut, int nombre) {
        // renvoyer nombre éléments de la liste, commençant à la position debut
        if(liste == null){
            throw new IllegalArgumentException("La liste categorie est vide");
        }
        if(debut>liste.size() || nombre < 0){
            throw new IllegalArgumentException("Parametes inavalides");
        }
        List<Categorie> newliste = new ArrayList();
        int taille = (debut+nombre)>liste.size()?liste.size():(debut+nombre);
        for (int i = debut; i < taille ; i++) {
            newliste.add(liste.get(i));
        }
        return newliste;
    }

}
