/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Achat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author TKM
 */
public class AchatService {
    
    static List<Achat> liste = new ArrayList<>();


    public static void ajouter(Achat e) {
        // ajoute l'objet e dans la collection liste
        if(e == null){
            throw new IllegalArgumentException("L'ojbet Achat est null ");
        }
        liste.add(e);
    }

    public static void modifier(Achat e) {
        // remplace par e, l'objet Achat de la liste qui a même id que e
        if(e == null){
            throw new IllegalArgumentException("L'ojbet Achat est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), e.getId())){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Achat avec l'id " +e.getId()+ " n'est pas trouvé");
        }
        liste.set(pos, e);
    }

    public static Achat trouver(Long id) {
        // renvoie l'objet Achat de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id Achat est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Achat avec l'id " +id+ " n'est pas trouvé");
        }
        
        return liste.get(pos);
    }

    public static void supprimer(Long id) {
        // retirer de la liste, l'objet Achat qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("Le champ id Achat est null ");
        }
        int pos=-1;
        for (int i=0;i<liste.size();i++) {
            if(Objects.equals(liste.get(i).getId(), id)){
                pos = i;
            }
        }
        if(pos == -1){
            throw new IllegalArgumentException("L'objet Achat avec l'id " +id+ " n'est pas trouvé");
        }
        
        liste.remove(pos);
    }

    public static void supprimer(Achat e) {
        // retirer de la liste, l'objet Achat passé en paramètre
        supprimer(e.getId());
    }

    public static List<Achat> lister() {
        // renvoyer tous les éléments de la liste
        if(liste == null){
            throw new IllegalArgumentException("La liste Achat est vide");
        }
        return liste;
    }

    public static List<Achat> lister(int debut, int nombre) {
        // renvoyer nombre éléments de la liste, commençant à la position debut
        if(liste == null){
            throw new IllegalArgumentException("La liste Achat est vide");
        }
        if(debut>liste.size() || nombre < 0){
            throw new IllegalArgumentException("Parametes inavalides");
        }
        List<Achat> newliste = new ArrayList();
        int taille = (debut+nombre)>liste.size()?liste.size():(debut+nombre);
        for (int i = debut; i < taille ; i++) {
            newliste.add(liste.get(i));
        }
        return newliste;
    }
}
