<div align="center" id="top"> 

  &#xa0;

  <!-- <a href="https://ulboard_mobile.netlify.app">Demo</a> -->
</div>

<h1 align="center">Boutique Jax Rs</h1>

<p align="center">
  <img alt="License" src="https://img.shields.io/gitlab/license/tkmmoise/boutique_jax-rs?color=%2353B0AB">

  <!-- <img alt="Github issues" src="https://img.shields.io/github/issues/tkmmoise/ULMobile?color=56BEB8" /> -->

  <!-- <img alt="Github forks" src="https://img.shields.io/github/forks/tkmmoise/ULMobile?color=56BEB8" /> -->

  <!-- <img alt="Github stars" src="https://img.shields.io/github/stars/tkmmoise/ULMobile?color=56BEB8" /> -->
</p>

<!-- Status -->

<!-- <h4 align="center"> 
	🚧  Ulboard_mobile 🚀 Under construction...  🚧
</h4> 

<hr> -->

<p align="center">
  <a href="#dart-about">Description</a> &#xa0; | &#xa0; 
  <a href="#sparkles-features">Fonctionnalités</a> &#xa0; | &#xa0;
  <a href="#rocket-technologies">Technologies</a> &#xa0; | &#xa0;
  <a href="#white_check_mark-requirements">Conditions</a> &#xa0; | &#xa0;
  <a href="#memo-license">Licence</a> &#xa0; | &#xa0;
  <a href="https://github.com/tkmmoise" target="_blank">Auteur</a>
</p>

<br>

## :dart: Description ##

Ce projet est un simple TP realisé pendant le cours de programmation distribuée avec JAX-RS, une API conçue pour implémenter des API WEB aussi appelées Web Services RESTful.


## :sparkles: Fonctionnalités ##

:heavy_check_mark: Create, Read, Update, Delete (CRUD);

## :rocket: Technologies ##

Les outils et technologies suivants ont été utilisés dans ce projet :

- [NetBeans](https://netbeans.apache.org/download/index.html)
- [Java](https://www.java.com/)
- [Tomcat](https://tomcat.apache.org/)
- [Java 11 JDK](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html)
- [JAX-RS](https://docs.oracle.com/javaee/6/tutorial/doc/giepu.html)

## :white_check_mark: Conditions ##

Avant de démarrer :checkered_flag:, vous devez avoir installé [Java 11 JDK](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html), [Tomcat](https://tomcat.apache.org/) et [NetBeans](https://netbeans.apache.org/download/index.html).


## :memo: Licence ##

Ce projet est sous licence du MIT. Pour plus de détails, consultez le fichier [LICENSE](LICENSE.md).


Coder avec :heart: par <a href="https://github.com/tkmmoise" target="_blank">Moses</a>

&#xa0;

<a href="#top">Haut de la page</a>
